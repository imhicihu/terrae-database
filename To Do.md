* automatic installer of WinIsis _vía_ [Chocolatey](https://chocolatey.org/)
* create a kind of `ROADMAP` to visualize the steps to do / done up to now
* ~~Migrate Del.icio.us gathered links to Evernote~~
* ~~uploads photos taken from the brugge notebook~~
* ~~backup `subsuelo.zip` to `Dropbox` & `Google Drive` via `IFFTTT`~~
* ~~Choose a licence and put it on README (https://chooselicence.org)~~
* ~~file verification backup & compression:~~
	![3929066438-subsuelo-compress-ok.png](https://bitbucket.org/repo/EBnakg/images/1962475127-3929066438-subsuelo-compress-ok.png)
* ~~capture Tabla de Campos (CDS): `order`, `number`, `type`~~
* ~~take photos of the books that contain reference to Terrae. Then send it back to Ricardo.~~